## Pregunta 7
Al instanciar tiene que buscar un espacio de memoria dsponible o hacer espacio para poder crear el objeto y al eliminar objetos (borrarlos de escena) despeja la memoria en donde se encontraba �ste, mas no la reordena para permitir un mejor manejo de �sta.

Por ese motivo es recomendable crear un Pool meidanamente grande de enemigos de donde se tomar� uno de los enemigos desactivados activ�ndolo y situ�ndolo donde se necesite. Estos enemigos al morir �nicamente se desactivar�n para que no se carguen ni se tomen en cuenta en la escena.

Si fuese necesario instanciar otro enemigo pero todo el Pool se encuentra activo, entonces se crear� uno nuevo y se a�adir� al Pool.