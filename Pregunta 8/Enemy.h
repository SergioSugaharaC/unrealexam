#pragma once
#include <string>
#include <vector>
#include <iostream>
#include <time.h>
#include "Attack.h"

using namespace std;

class Enemy{
private:
	string name;
	string enemyTipe;
	vector<Attack> enemyAttack;
	bool attacking;
	clock_t cooldown;

public:
	string Name() { return this->name; }
	string Type() { return this->enemyTipe; }
	bool Attacking() { return this->attacking; }
	vector<Attack> GetEnemyAttack() { return this->enemyAttack; }

	Enemy(string n, string t) {
		this->name = n;
		this->enemyTipe = t;
		this->attacking = false;
	}

public:
	void addAttack(float sec, vector<string> attTag, vector<string> attReact) {
		this->enemyAttack.push_back(Attack(sec, attTag, attReact));
	}

	void attack() {
		cout << this->name << "is trying to attack" << endl;
		cooldown = clock();
		attacking = true;
	}

	void Update() {
		if ((clock() - cooldown) / CLOCKS_PER_SEC == enemyAttack.at(0).Duration()) {
			attacking = false;
		}
	}
};

