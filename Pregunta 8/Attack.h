#pragma once
#include<string>
#include<vector>

using namespace std;

class Attack {
private:
	float attackDuration;
	vector<string> attackTags;
	vector<string> reactsTags;

public:
	float Duration() { return this->attackDuration; }
	vector<string> Tags() { return this->attackTags; }
	vector<string> Reacts() { return this->reactsTags; }

	Attack(float sec, vector<string> attTag, vector<string> attReact) {
		this->attackDuration = sec;
		this->attackTags = attTag;
		this->reactsTags = attReact;
	}
};