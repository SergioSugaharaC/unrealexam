#pragma once
#include<vector>
#include<time.h>
#include "Enemy.h"

class Director {
private:
	int numAttacks;
	clock_t elapsed;
	bool canOrchest;
	Enemy *lastAtt;

	int R; // Probability range
	int N; // Number of attacks before orchestry
	int T; // Time between orchestry attacks
	vector<Enemy*> activeEnemies; // List of enemies in scene

public:
	int setN(int n) { this->N = n; }
	int setR(int r) { this->R = r; }
	int setT(int t) { this->T = t; }

	Director(int r, int n, int t) {
		this->N = n;
		this->R = r;
		this->T = t;
		this->numAttacks = 0;
		this->elapsed = 0;
		this->canOrchest = true;
		this->lastAtt = nullptr;
	}

public:
	void AddEnemies() {
		Enemy* Enem;
		vector<string> tags;
		vector<string> reacts;

		Enem = new Enemy("Rana","Terrestre");
		tags = { "flincher","knockdown" };
		reacts = { "bullet" };
		Enem->addAttack(5, tags, reacts);
		activeEnemies.push_back(Enem);

		Enem = new Enemy("Tronco","Terrestre");
		tags = { "charger","distance" };
		reacts = { "knockdown" };
		Enem->addAttack(8, tags, reacts);
		activeEnemies.push_back(Enem);
		//cout << "Enemy added " << activeEnemies.at(1)->GetEnemyAttack().at(0).Reacts().at(0) << endl;

		Enem = new Enemy("Mono","Aereo");
		tags = { "distance","bullet" };
		reacts = { "charger" };
		Enem->addAttack(2, tags, reacts);
		activeEnemies.push_back(Enem);
		//cout << "Enemy added " << activeEnemies.at(2)->GetEnemyAttack().at(0).Reacts().at(0) << endl;
		cout << "Enemies Added" << endl;
	}

	void SelectAttack(int enem) {
		if (lastAtt != nullptr) {
			if (activeEnemies.at(enem)->Type() != lastAtt->Type())
				activeEnemies.at(enem)->attack();
			else
				cout << "blocked by " << lastAtt->Name() << endl;
		} else {
			activeEnemies.at(enem)->attack();
		}
		
		lastAtt = activeEnemies.at(enem);
		numAttacks++;
	}

	void Update() {
		for (auto& enem : activeEnemies) {
			enem->Update();

			if(lastAtt != nullptr)
				if (enem->Name() == lastAtt->Name() && !enem->Attacking()) {
					lastAtt = nullptr;
					cout << "the attack blocker from " << enem->Name() << " has ended" << endl;
				}
		}

		canOrchest = (numAttacks >= N && (clock() - elapsed) / CLOCKS_PER_SEC >= T);
		
		// elapsed = clock(); <- when havo to reset
	}
};