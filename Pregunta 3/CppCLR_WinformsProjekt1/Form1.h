#pragma once
#include <math.h>

namespace CppCLRWinformsProjekt {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Zusammenfassung f�r Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form{
	public:
		Form1(void){
			InitializeComponent();
			//
			//TODO: Konstruktorcode hier hinzuf�gen.
			//
			grapher = this->CreateGraphics();
		}

	protected:
		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		~Form1(){
			if (components){ delete components;	}
			if (grapher) { delete grapher; }
		}
	private: System::Windows::Forms::Label^ lblRadio;
	private: System::Windows::Forms::Label^ lblPuntos;
	private: System::Windows::Forms::TextBox^ txtRadio;
	private: System::Windows::Forms::TextBox^ txtPuntos;
	private: System::Windows::Forms::Label^ lblCentroX;
	private: System::Windows::Forms::Label^ lblCentroY;
	private: System::Windows::Forms::TextBox^ txtCentroY;
	private: System::Windows::Forms::TextBox^ txtCentroX;
	private: System::Windows::Forms::Button^ btnDraw;
	
	protected:

	private:
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		System::ComponentModel::Container ^components;
		System::Drawing::Graphics^ grapher;

		//void DrawString() {
		//	Graphics formGraphics = this->CreateGraphics();
		//	string drawString = "Sample Text";
		//	Font drawFont = Font("Arial", 16);
		//	SolidBrush drawBrush = SolidBrush(Color::Black);
		//	float x = 150.0F;
		//	float y = 50.0F;
		//	System::Drawing::StringFormat drawFormat = new System::Drawing::StringFormat();
		//	formGraphics.DrawString(drawString, drawFont, drawBrush, x, y, drawFormat);
		//	drawFont.Dispose();
		//	drawBrush.Dispose();
		//	formGraphics.Dispose();
		//}

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		void InitializeComponent(void)
		{
			this->lblRadio = (gcnew System::Windows::Forms::Label());
			this->lblPuntos = (gcnew System::Windows::Forms::Label());
			this->txtRadio = (gcnew System::Windows::Forms::TextBox());
			this->txtPuntos = (gcnew System::Windows::Forms::TextBox());
			this->lblCentroX = (gcnew System::Windows::Forms::Label());
			this->lblCentroY = (gcnew System::Windows::Forms::Label());
			this->txtCentroY = (gcnew System::Windows::Forms::TextBox());
			this->txtCentroX = (gcnew System::Windows::Forms::TextBox());
			this->btnDraw = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// lblRadio
			// 
			this->lblRadio->AutoSize = true;
			this->lblRadio->Location = System::Drawing::Point(13, 9);
			this->lblRadio->Name = L"lblRadio";
			this->lblRadio->Size = System::Drawing::Size(35, 13);
			this->lblRadio->TabIndex = 0;
			this->lblRadio->Text = L"Radio";
			// 
			// lblPuntos
			// 
			this->lblPuntos->AutoSize = true;
			this->lblPuntos->Location = System::Drawing::Point(13, 41);
			this->lblPuntos->Name = L"lblPuntos";
			this->lblPuntos->Size = System::Drawing::Size(50, 13);
			this->lblPuntos->TabIndex = 1;
			this->lblPuntos->Text = L"# Puntos";
			// 
			// txtRadio
			// 
			this->txtRadio->Location = System::Drawing::Point(71, 6);
			this->txtRadio->Name = L"txtRadio";
			this->txtRadio->Size = System::Drawing::Size(64, 20);
			this->txtRadio->TabIndex = 2;
			// 
			// txtPuntos
			// 
			this->txtPuntos->Location = System::Drawing::Point(71, 38);
			this->txtPuntos->Name = L"txtPuntos";
			this->txtPuntos->Size = System::Drawing::Size(64, 20);
			this->txtPuntos->TabIndex = 3;
			// 
			// lblCentroX
			// 
			this->lblCentroX->AutoSize = true;
			this->lblCentroX->Location = System::Drawing::Point(157, 9);
			this->lblCentroX->Name = L"lblCentroX";
			this->lblCentroX->Size = System::Drawing::Size(45, 13);
			this->lblCentroX->TabIndex = 4;
			this->lblCentroX->Text = L"CentroX";
			// 
			// lblCentroY
			// 
			this->lblCentroY->AutoSize = true;
			this->lblCentroY->Location = System::Drawing::Point(157, 41);
			this->lblCentroY->Name = L"lblCentroY";
			this->lblCentroY->Size = System::Drawing::Size(45, 13);
			this->lblCentroY->TabIndex = 5;
			this->lblCentroY->Text = L"CentroY";
			// 
			// txtCentroY
			// 
			this->txtCentroY->Location = System::Drawing::Point(208, 38);
			this->txtCentroY->Name = L"txtCentroY";
			this->txtCentroY->Size = System::Drawing::Size(64, 20);
			this->txtCentroY->TabIndex = 7;
			// 
			// txtCentroX
			// 
			this->txtCentroX->Location = System::Drawing::Point(208, 6);
			this->txtCentroX->Name = L"txtCentroX";
			this->txtCentroX->Size = System::Drawing::Size(64, 20);
			this->txtCentroX->TabIndex = 6;
			// 
			// btnDraw
			// 
			this->btnDraw->Location = System::Drawing::Point(13, 64);
			this->btnDraw->Name = L"btnDraw";
			this->btnDraw->Size = System::Drawing::Size(259, 23);
			this->btnDraw->TabIndex = 8;
			this->btnDraw->Text = L"Draw Semicircunference";
			this->btnDraw->UseVisualStyleBackColor = true;
			this->btnDraw->Click += gcnew System::EventHandler(this, &Form1::btnDraw_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->btnDraw);
			this->Controls->Add(this->txtCentroY);
			this->Controls->Add(this->txtCentroX);
			this->Controls->Add(this->lblCentroY);
			this->Controls->Add(this->lblCentroX);
			this->Controls->Add(this->txtPuntos);
			this->Controls->Add(this->txtRadio);
			this->Controls->Add(this->lblPuntos);
			this->Controls->Add(this->lblRadio);
			this->Name = L"Form1";
			this->Text = L"Form1";
			this->ResumeLayout(false);
			this->PerformLayout();
		}

#pragma endregion
	private: System::Void Form1_Paint(System::Object^ sender, System::Windows::Forms::PaintEventArgs^ e) {
	}

	private: System::Void btnDraw_Click(System::Object^ sender, System::EventArgs^ e) {
		   int centerX = Convert::ToInt16(txtCentroX->Text) + Height / 2;
		   int centerY = Convert::ToInt16(txtCentroY->Text) +  Width / 2;
		   int nPoints = Convert::ToInt32(txtPuntos->Text);
		double radius  = Convert::ToDouble(txtRadio->Text);
		
		float* anglesArr = new float[nPoints + 1];
		float* posXArr   = new float[nPoints + 1];
		float* posYArr   = new float[nPoints + 1];

		anglesArr[0] = 0;
		  posXArr[0] = 0;
		  posYArr[0] = 0;

		for (int i = 1; i <= nPoints + 1; i++) {
			anglesArr[i] = ((i - 1) * (180 / (nPoints - 1))) * Math::PI / 180;
			posXArr[i] = radius * cos(anglesArr[i]);
			posYArr[i] = radius * sin(anglesArr[i]);
		}

		for (int i = 0; i <= nPoints; i++){
			this->grapher->DrawEllipse(Pens::Red, (int)(posXArr[i] + centerX), (int)(posYArr[i] + centerY), 5, 5);
		}
	}
	};

};

