## Pregunta 6
```python
## Global Variables (Independent of Classes)
activeEnemies = [EnemA, EnemB, EnemC, EnemD, ...]
N = 3 # Max Number of Ricochets
B = 0 # Number of Bounces Made
X = 10 # Chaining Range

## Called Every Timespan T Since Last 
 # UpdateEnemiesList() Function Call
def Update():
  elapsed += deltaTime
  if elapsed > T:
    UpdateEnemiesList()

## Check for Alive Enemies and Add Them to the List 
def UpdateEnemiesList():
  activeEnemies = FindEnemiesAliveInScene()
  elapsed = 0

## Called on Enemy Class Whenever it Takes a Melee Attack
def OnMeleeAttackConnected():
  chance = 0 # Percent Chance to Ricochet
  if B == 0:
    chance = 25
  else:
    chance = 10

  chaining = False
  if Random(0, 100) < chance:
    chaining = True
    # Will substract self from the list
    acttiveEnemies.pop(self)
    while chaining:
      randEnem = activeEnemies[Random(0, activeEnemies.Length)]
      # Will substract randEnem from the list
      activeEnemies.pop(randEnem) 

      # Will determine if in range
      if Distance(self, randEnem) <= X:
        randEnem.DoDamage(10) # Reduce randEnem Health
        ## Will be Considered as a Melee Hit, so it Triggers 
         # the OnMeleeAttackConnected() method of randEnem
        player.MeleeAttack(randEnem)
        chaining = False

      ## If There are no More Enemies in Range Stop
       # Looking for Random Enemy to Bounce
      if activeEnemies.Length == 0:
        chaining = False
        UpdateEnemiesList()
```