#include <vector>
#include <iostream>
#include "Rectangle.h"

using namespace std;

void main() {
	vector<Rectangle> rectangles;
	rectangles.push_back(Rectangle(100, 100, 100, 100)); //Rectangle A
	rectangles.push_back(Rectangle(100, 200,  80, 120)); //Rectangle B
	rectangles.push_back(Rectangle(200, 200, 140,  80)); //Rectangle C

	for (int i = 0; i < rectangles.size() - 1; i++) {
		Rectangle *rect = &rectangles.at(i);
		float rectR = rect->getX() + rect->getW() / 2;
		float rectL = rect->getX() - rect->getW() / 2;
		float rectT = rect->getY() - rect->getH() / 2;
		float rectB = rect->getY() + rect->getH() / 2;

		for (int j = i + 1; j < rectangles.size(); j++) {
			Rectangle *comp = &rectangles.at(j);
			float compR = comp->getX() + comp->getW() / 2;
			float compL = comp->getX() - comp->getW() / 2;
			float compT = comp->getY() - comp->getH() / 2;
			float compB = comp->getY() + comp->getH() / 2;
			
			cout << i << "," << j << endl;
			cout << rectR << "," << compR << endl;
			cout << rectL << "," << compL << endl;
			cout << rectT << "," << compT << endl;
			cout << rectB << "," << compB << endl;

			if((rectL <= compL && compL <= rectR) || (rectL <= compR && compR <= rectR)){
				if ((rectT <= compT && compT <= rectB) || (rectT <= compB && compB <= rectB)) {
					cout << "hubo colision" << endl;
				}
			}
		}
	}
}