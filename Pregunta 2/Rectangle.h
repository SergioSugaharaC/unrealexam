#pragma once
class Rectangle{
private: 
	float posX, posY;
	float width, height;
	
public:
	Rectangle(float x, float y, float w, float h) {
		posX = x;
		posY = y;
		width = w;
		height = h;
	};

	float getX() const { return this->posX; }
	float getY() const { return this->posY; }
	float getW() const { return this->width; }
	float getH() const { return this->height; }
};

